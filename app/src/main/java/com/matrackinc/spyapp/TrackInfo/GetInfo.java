package com.matrackinc.spyapp.TrackInfo;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;

import com.matrackinc.spyapp.DataModal.LogModal;
import com.matrackinc.spyapp.DataModal.ResponseData;
import com.matrackinc.spyapp.DatabaseHandler.DataBaseClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by BijinaPV on 06-12-2017.
 */

public class GetInfo {

    Context mContext;
    DataBaseClass Dbhandler;
    SimpleDateFormat TimeFormat = new SimpleDateFormat("HH:mm:ss z");
    public GetInfo(Context context)
    {
        this.mContext = context;
        Dbhandler = new DataBaseClass(mContext);
    }
    public void GetCallLog() {

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {

                    return;
                } else {
                    Cursor CallCursor = mContext.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);

                    int type = CallCursor.getColumnIndex(CallLog.Calls.TYPE);
                    int date = CallCursor.getColumnIndex(CallLog.Calls.DATE);
                    CallCursor.moveToNext();

                    String callType = CallCursor.getString(type);
                    String callDate = CallCursor.getString(date);
                    Date callDayTime = new Date(Long.valueOf(callDate));

                    String time=TimeFormat.format(callDayTime);

                    String call_type = null;
                    int call_code = Integer.parseInt(callType);
                    switch (call_code)
                    {
                        case CallLog.Calls.OUTGOING_TYPE:
                            call_type = "OUTGOING";
                            break;

                        case CallLog.Calls.INCOMING_TYPE:
                            call_type = "INCOMING";
                            break;

                        case CallLog.Calls.MISSED_TYPE:
                            call_type = "MISSED";
                            break;
                    }
//                    Dbhandler.InsertLog("call",time,call_type);
//                    Dbhandler.GetLogInfo();
//                    CallCursor.close();
                }
            }
        }



