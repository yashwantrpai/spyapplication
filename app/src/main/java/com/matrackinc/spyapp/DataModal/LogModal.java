package com.matrackinc.spyapp.DataModal;

/**
 * Created by BijinaPV on 07-12-2017.
 */

public class LogModal {

    private String _LogID;
    private String _time;
    private String _type;
    private String log_type;
    private String start_time;
    private String end_time;

    public String getStartTime(){return start_time;}
    public String getEndTime(){return end_time;}
    public String getTime(){
        return _time;
    }
    public String getLogId(){
        return _LogID;
    }
    public void setTime(String call_time){
        _time = call_time;
    }
    public String getlogType(){
        return log_type;
    }
    public void setStart_time(String Start_Time)
    {
        start_time=Start_Time;
    }
    public void setLogId(String logid)
    {
        _LogID=logid;
    }
    public void setEnd_time(String End_Time)
    {
        end_time=End_Time;
    }
    public void setLogType(String logType){
        log_type = logType;
    }
    public String getType(){
        return _type;
    }
    public void setType(String _type_){
        _type = _type_;
    }


}
