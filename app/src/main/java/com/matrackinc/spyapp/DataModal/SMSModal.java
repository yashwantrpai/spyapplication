package com.matrackinc.spyapp.DataModal;

/**
 * Created by BijinaPV on 06-12-2017.
 */

public class SMSModal {
    private String _id;
    private String _address;
    private String _msg;
    private String _date;
    private String _time;
    private String _folderName;
    private String _from;
    private String _to;
    public String getId(){
        return _id;
    }
    public String getAddress(){
        return _address;
    }
    public String getFrom(){
        return _from;
    }
    public String getTo()
    {
        return _to;
    }
    public String getMsg(){
        return _msg;
    }
    public String getDate(){
        return _date;
    }
    public String getTime(){
        return _time;
    }
    public String getFolderName(){
        return _folderName;
    }
    public void setId(String id){
        _id = id;
    }
    public void setfrom(String from_)
    {
        _from=from_;
    }
    public void setAddress(String address){
        _address = address;
    }
    public void setMsg(String msg){
        _msg = msg;
    }
    public void setDate(String date_){
        _date = date_;
    }
    public void setTo(String to_)
    {
        _to=to_;
    }
    public void setTime(String time){
        _time = time;
    }
    public void setFolderName(String folderName){
        _folderName = folderName;
    }
}
