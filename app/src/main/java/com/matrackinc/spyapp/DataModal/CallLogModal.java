package com.matrackinc.spyapp.DataModal;

/**
 * Created by BijinaPV on 07-12-2017.
 */

public class CallLogModal {
    private String _ph_no;
    private String _call_type;
    private String _call_date;
    private String _call_duartion;
    private String _call_time;
    private String _call_from;
    private String _call_to;
    public String getphonenumber(){
        return _ph_no;
    }
    public String getCallType(){
        return _call_type;
    }
    public String getCallFrom()
    {
        return _call_from;
    }
    public String getCallTo()
    {
        return _call_to;
    }
    public String getCallDate(){
        return _call_date;
    }
    public String getCallTime(){
        return _call_time;
    }
    public String getCallduration(){
        return _call_duartion;
    }
    public void setCallFrom(String from_)
    {
        _call_from=from_;
    }
    public void setCallTo(String To_)
    {
        _call_to=To_;
    }

    public void setphoneNumber(String Ph_no){
        this._ph_no = Ph_no;
    }
    public void setcallType(String call_type){
        _call_type = call_type;
    }
    public void setCallDate(String call_date){
        _call_date = call_date;
    }
    public void setCallduration(String call_duartion){
        _call_duartion = call_duartion;
    }
    public void setCallTime(String call_time){
        _call_time = call_time;
    }


}
