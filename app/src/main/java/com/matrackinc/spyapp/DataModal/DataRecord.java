package com.matrackinc.spyapp.DataModal;

/**
 * Created by Rajendra on 12/9/17.
 */

public class DataRecord {

    private int id;
    private String data;
    private String created_time;
    private Boolean uploaded;

    public DataRecord(){
    }

    public DataRecord (int _id, String data, String created_time, Boolean uploaded) {
        this.id = id;
        this.data = data;
        this.created_time = created_time;
        this.uploaded = uploaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int _id) { this.id = _id; }

    public String getData() {
        return data;
    }

    public void setData(String json) {
        this.data = json;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public Boolean getUploaded() {
        return uploaded;
    }

    public void setUploaded(Boolean uploaded) {
        this.uploaded = uploaded;
    }
}
