package com.matrackinc.spyapp.Utils;

import android.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by Amol on 12/8/17.
 *
 */

public class CryptoUtil {

    private static String publicKeyString =  "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0UlGHoC0MjUC4EPvj6lf\n" +
            "4xX/ENqTLTPpFr5bSHkJJgrseHy7HmgCxKko0vzlPMa39x5OXt+Ykzfk3PhNhKF2\n" +
            "eLy24dUf+H//8odAkmb8u7SQtYr3dgBvUoRdIYcUzmuiTqkR0bh+WFK1cU+cEdAI\n" +
            "bqnsbrGG2U6nw6Bxlv1Q2yZ8CpeGTA2ycUUgrayiu2XqhTOd9ipM+z0qMRyXwXAN\n" +
            "M+tkiYtapD00E/SnilwAP9EhkFdKGb8nAtGNdASSmC3kAwYC2uaPdb/wYYEvMt92\n" +
            "q8o8QL1jKXUrJcPk9c3CRKhvMsFcIzQdzWHRckIpWyeQ33PTYgFPlEZqM/+x6xkS\n" +
            "VwIDAQAB";

    private static String privateKeyString = "MIIEpAIBAAKCAQEA0UlGHoC0MjUC4EPvj6lf4xX/ENqTLTPpFr5bSHkJJgrseHy7\n" +
            "HmgCxKko0vzlPMa39x5OXt+Ykzfk3PhNhKF2eLy24dUf+H//8odAkmb8u7SQtYr3\n" +
            "dgBvUoRdIYcUzmuiTqkR0bh+WFK1cU+cEdAIbqnsbrGG2U6nw6Bxlv1Q2yZ8CpeG\n" +
            "TA2ycUUgrayiu2XqhTOd9ipM+z0qMRyXwXANM+tkiYtapD00E/SnilwAP9EhkFdK\n" +
            "Gb8nAtGNdASSmC3kAwYC2uaPdb/wYYEvMt92q8o8QL1jKXUrJcPk9c3CRKhvMsFc\n" +
            "IzQdzWHRckIpWyeQ33PTYgFPlEZqM/+x6xkSVwIDAQABAoIBAGCU1AsXkhDpbd5Y\n" +
            "+6DspReC+yan9+uQXSzlw/J6CMLJn78qsJ+jmENxXSmhE5ksi5E7JyeZQCwQS0m+\n" +
            "POfMK4QkcrFdTVe8mj0uNZpadJ+bAH0HGJQYN0d5VhE26V8pRMnQNCz4WzYfVSRp\n" +
            "fLirrHiFalrbCkQWjE03KbRoSnwXPG8Xr23en96iSSl9hi0k/fDFVcs5coquBA49\n" +
            "6fhQMzn7eDTCNvEU9lFqxxJfIrTbUH9b7JUSmtTCb+l00iWTluW/gU7RF8e4jm85\n" +
            "NNkPaCtCshLSXi2O9zm/BfJq1e+dkcss55Wh/BMv+uy5Cgdjg8KFY5odZYJBf2lM\n" +
            "COMi08ECgYEA9oQxWfXWdXlq9HOfBDclw/5bKYekd5lFZAuORAPHMV5HibN21Tx6\n" +
            "FHt1sk+sgDV4DoM8993INZYrZp1iSHxIJjIFi9cZAhAIMiI9eTTngPx2Q5now0hO\n" +
            "kfiSpVD6O6Gb+yAxeQQFAUTrtNp76BX8yGsL86G3hxHAHjUix57fMR8CgYEA2VZr\n" +
            "37P8JCxqV3ccNv2AwlwoIn2hPlddmHhbGSXSeVtsgy0TGgOLIKAJd3iX0wa97QGN\n" +
            "vlWRcA78uIe0cNJ6iw1Q/LNEtdnHrAiNlp7TWU5m/PkABZ+O6HdBKhkWfnTWM3cY\n" +
            "67vu3lN5JBp3aQy2fPfVJkgM15RgS0t0Tzw/X8kCgYEAiCSlpMsyb5poySx5q7hR\n" +
            "MRbufdcdq7eILSJ8pW94uPkahSAKBuXpfhpY++sol33wGgO8zh88DOwsd/V/bAzH\n" +
            "Q+DoUzyt4Qe9nwJt9NyU5hWHsvnzJLZwPgmL46T/LaOpI126NN1VbkgD7NOlDp3m\n" +
            "B0GnThwH4Z6Z/ZTW6mwBU30CgYEA1hqu9pfmwvQGZtfuMbYWjGIhH6lHmZSTncqj\n" +
            "Y+jAqdEI20kiUjQ/cQp3iPyCBN/VZpBrRKAjQJNjH56Agcy38BhUOj2YaNV8QNvE\n" +
            "MCeS6Z21E21xOhjvvc8NoR1J7ZXIFrczE7f2H4t4AbsY+6IMfJvhnW7JR+7h1gsl\n" +
            "L5GYlRECgYBX6MH3dzy1Ka4A5KDEtL27BbJG6USSSK8fDyrNTATYU+mm8PeAeZ8Y\n" +
            "Bj7EBXeAan+BXkaTPNLQ6P1Zavj0bWsdX4ENNk3W12KaMPeRzp8S09Qtlrx1/kMd\n" +
            "+JQnhigfgiXqbIS+qQOmcsb5l8SHUu8JvF+P7+FNp7qkifwIXfGNkw==";

    private PublicKey publicKey ;
    private PrivateKey privateKey;

    public CryptoUtil(){
        //generateKey();
    }

//    private void generateKey() {
//        try
//        {
//            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
//            keyPairGenerator.initialize(2048);
//            KeyPair keyPair = keyPairGenerator.genKeyPair();
//            publicKey = keyPair.getPublic();
//            privateKey = keyPair.getPrivate();
//
//            byte[] publicKeyBytes =  publicKey.getEncoded();
//            byte[] privateKeyBytes =  privateKey.getEncoded();
//
//            publicKeyString = Base64.encodeToString(publicKeyBytes, Base64.DEFAULT);
//            privateKeyString = Base64.encodeToString(privateKeyBytes, Base64.DEFAULT);
//            android.util.Log.i("Public Key :", publicKeyString);
//            android.util.Log.i("Private Key :", privateKeyString);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//    }

    public byte[] encrypt(String jsonData)
    {
        try {

            byte[] plainText = jsonData.getBytes();

            byte[] publicKeyBytes = Base64.decode(publicKeyString, Base64.DEFAULT);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyBytes);
            publicKey = keyFactory.generatePublic(spec);

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher.doFinal(plainText);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (BadPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    public byte[] decrypt(String jsonData)
    {
        try {
            byte[] encryptedBytes = Base64.decode(jsonData, Base64.DEFAULT);

            byte[] privateKeyBytes = Base64.decode(privateKeyString, Base64.DEFAULT);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateKeyBytes);
            privateKey = keyFactory.generatePrivate(spec);

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return cipher.doFinal(encryptedBytes);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (BadPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }
}
