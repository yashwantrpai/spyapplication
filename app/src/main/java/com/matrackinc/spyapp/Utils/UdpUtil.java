package com.matrackinc.spyapp.Utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by Amol on 12/11/17.
 *
 */

public class UdpUtil {

    public UdpUtil(){

    }

    public void handleUdpRequest(Enum endPoints, String encryptedData, final UdpUtilCallback udpUtilCallback) {
        try
        {
            URL url = null;
            String enumValue = endPoints.toString();


            switch (enumValue) {

                case "saveData": {
                    url = new URL("http://23.239.28.100/mydealership/spy/upd_threaded_old.py");

                    String relativeUrl = url.toString();
                    UdpUtil.UdpAsync udpAsync = new UdpUtil.UdpAsync(new UdpUtilCallback() {
                        @Override
                        public void onCallbackComplete(String result) {
                            udpUtilCallback.onCallbackComplete(result);
                        }
                    });
                    udpAsync.execute(relativeUrl, encryptedData);
                    Log.d("server url", relativeUrl);
                    Log.d("sending data", encryptedData);
                    break;
                }

//                case "login": {
//                    url = new URL("http://google.com");
//                    String relativeUrl = url.toString();
//                    UdpUtil.UdpAsync udpAsync = new UdpUtil.UdpAsync(new UdpUtilCallback() {
//                        @Override
//                        public void onCallbackComplete(String result) {
//                            udpUtilCallback.onCallbackComplete(result);
//                        }
//                    });
//                    udpAsync.execute(relativeUrl, encryptedData);
//                    break;
//                }
            }
        }
        catch(MalformedURLException e){
            e.printStackTrace();
        }
    }

    private static class UdpAsync extends AsyncTask<String, Void, String> {

        private UdpUtilCallback _UdpUtil_callback = null;

        private UdpAsync(UdpUtilCallback udpUtilCallback){
            _UdpUtil_callback = udpUtilCallback;
        }

        @Override
        protected String doInBackground(String... params) {
            try
            {
                URL relativeUrl = new URL(params[0]);
                String url = relativeUrl.toString();
                String data = params[1];


                DatagramSocket socket = new DatagramSocket();



                int portNumber = 2555;
                InetAddress IPAddress = InetAddress.getByName("23.239.28.100");
                byte[] sendData;
                sendData = data.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, portNumber);
                socket.send(sendPacket);
                socket.close();




            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;//replace with response
        }

        protected void onPostExecute(String result){
            super.onPostExecute(result);
            Log.d("-----------------","Callback Response "+result);
            if (_UdpUtil_callback != null) _UdpUtil_callback.onCallbackComplete(result);
        }
    }

}
