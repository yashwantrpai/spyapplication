package com.matrackinc.spyapp.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.matrackinc.spyapp.DataModal.DataRecord;

import java.util.LinkedList;
import java.util.List;

public class DatabaseUtil extends SQLiteOpenHelper
{
    private static int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "LocalData";
    private static String TABLE_NAME = "DatabaseDataRecords";
    private static String ID = "_id";
    private static String DATA = "data";
    private static String CREATED_TIME = "created_time";
    private static String UPLOADED = "uploaded";

    public DatabaseUtil(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATION_TABLE = "CREATE TABLE " + TABLE_NAME + " ( "
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT null, " +
                "created_time TEXT not null, uploaded TEXT not null);";

        sqLiteDatabase.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // you can implement here migration process
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        //this.onCreate(sqLiteDatabase);
    }

//    public void deleteOne(Object object) {
//        // Get reference to writable DB
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_NAME, "id = ?", new String[] { String.valueOf(object) });
//        db.close();
//    }
//
//    public Object getSingleRecord() {
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.query(TABLE_NAME, // a. table
//                COLUMNS, // b. column names
//                " id = ?", // c. selections
//                new String[] { String.valueOf(id) }, // d. selections args
//                null, // e. group by
//                null, // f. having
//                null, // g. order by
//                null); // h. limit
//
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        DataRecord record = new DataRecord();
//        record.setId(Integer.parseInt(cursor.getString(0)));
//        record.setJson(cursor.getString(1));
//        record.setCreated_time(cursor.getString(2));
//        record.setUploaded(Boolean.parseBoolean(cursor.getString(3)));
//
//        return record;
//    }

    public List<DataRecord> getNonUploadedRecords(){

        List<DataRecord> records = new LinkedList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + TABLE_NAME + " where uploaded = 0 ";
        Cursor cursor = db.rawQuery(query,null);

        DataRecord record = null;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                record = new DataRecord();
                record.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                record.setData(cursor.getString(cursor.getColumnIndex(DATA)));
                record.setCreated_time(cursor.getString(cursor.getColumnIndex(CREATED_TIME)));
                record.setUploaded(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(UPLOADED))));
                records.add(record);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return records;
    }

    public void saveRecord(DataRecord record) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DATA, record.getData());
        values.put(CREATED_TIME, record.getCreated_time());
        values.put(UPLOADED, record.getUploaded());
        db.insert(TABLE_NAME,null, values);
        db.close();
    }

    public int updateRecord(DataRecord record) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UPLOADED, false);

        int i = db.update(TABLE_NAME, // table
                values, // column/value
                "_id = ?", // selections
                new String[] { String.valueOf(record.getId()) });
        db.close();

        return i;
    }
}
