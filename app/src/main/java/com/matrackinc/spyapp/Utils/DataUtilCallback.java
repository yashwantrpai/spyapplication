package com.matrackinc.spyapp.Utils;

/**
 * Created by Amol on 12/11/17.
 *
 */

public interface DataUtilCallback {
    void onCallbackComplete(boolean success);
}
