package com.matrackinc.spyapp.Utils;

/**
 * Created by Amol on 12/11/17.
 *
 */

public interface ApiUtilCallback {
    void onCallbackComplete(String result);
}
