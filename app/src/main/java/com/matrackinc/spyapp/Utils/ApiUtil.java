package com.matrackinc.spyapp.Utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import static android.content.ContentValues.TAG;

/**
 * Created by Amol on 12/8/17.
 *
 */

public class ApiUtil
{
    public ApiUtil(){

    }

    public void handleRequest(Enum endPoints, String encryptedData, final ApiUtilCallback apiUtilCallback) {
        try
        {
            URL url = null;
            String enumValue = endPoints.toString();

            switch (enumValue) {

                case "saveData": {
                    Log.d("==========","savedData");
                    url = new URL("http://23.239.28.100/mydealership/spy/upd_threaded_old.py");
                    String relativeUrl = url.toString();
                    HttpPost post = new HttpPost(new ApiUtilCallback() {
                        @Override
                        public void onCallbackComplete(String result) {
                            apiUtilCallback.onCallbackComplete(result);
                        }
                    });
                    post.execute(relativeUrl, encryptedData);
                    break;
                }

                case "login": {
                    url = new URL("http://google.com");
                    String relativeUrl = url.toString();
                    HttpGet get = new HttpGet(new ApiUtilCallback() {
                        @Override
                        public void onCallbackComplete(String result) {
                            apiUtilCallback.onCallbackComplete(result);
                        }
                    });
                    get.execute(relativeUrl, encryptedData);
                    break;
                }
            }
        }
        catch(MalformedURLException e){
            e.printStackTrace();
        }
    }

    private class HttpGet extends AsyncTask<String, Void, String>{

        private static final String REQUEST_METHOD = "GET";
        private static final int READ_TIMEOUT = 15000;
        private static final int CONNECTION_TIMEOUT = 15000;

        private ApiUtilCallback _ApiUtil_callback = null;

        private HttpGet(ApiUtilCallback apiUtilCallback){
            _ApiUtil_callback = apiUtilCallback;
        }

        @Override
        protected String doInBackground(String... params) {
            String result;
            String inputLine;
            try
            {
                URL url = new URL(params[0]);

                //Create a connection
                HttpURLConnection connection =(HttpURLConnection) url.openConnection();

                //Set methods and timeouts
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                connection.connect();
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());

                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();

                while((inputLine = reader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }

                reader.close();
                streamReader.close();
                result = stringBuilder.toString();
            }
            catch(IOException e){
                e.printStackTrace();
                result = null;
            }

            return result;
        }
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if (_ApiUtil_callback != null) _ApiUtil_callback.onCallbackComplete(result);
        }
    }

    private class HttpPost extends AsyncTask<String, Void, String>{

        private ApiUtilCallback _ApiUtil_callback = null;

        private HttpPost(ApiUtilCallback apiUtilCallback){
            _ApiUtil_callback = apiUtilCallback;
        }

        @Override
        protected String doInBackground(String... params) {

            try
            {
                URL url = new URL(params[0]);
                String data = params[1];

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestMethod("POST");

                // OPTIONAL - Sets an authorization header
                //urlConnection.setRequestProperty("Authorization", "someAuthString");

                // Send the post body
                if (data != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(data);
                    writer.flush();
                }

                int statusCode = urlConnection.getResponseCode();

                if (statusCode ==  200) {

                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = new Scanner(inputStream,"UTF-8").useDelimiter("\\A").next();
                    //String response = convertInputStreamToString(inputStream);

                    // From here you can convert the string to JSON with whatever JSON parser you like to use
                    // After converting the string to JSON, I call my custom callback. You can follow this process too, or you can implement the onPostExecute(Result) method
                }
                else {
                    // Status code is not 200
                    // Do something to handle the error
                }

            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return null;
        }
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if (_ApiUtil_callback != null) _ApiUtil_callback.onCallbackComplete(result);
        }
    }
}
