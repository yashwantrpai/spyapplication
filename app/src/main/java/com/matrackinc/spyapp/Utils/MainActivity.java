package com.matrackinc.spyapp.Utils;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.matrackinc.spyapp.DataModal.DataRecord;
import com.matrackinc.spyapp.DatabaseHandler.DataBaseClass;
import com.matrackinc.spyapp.R;
import com.matrackinc.spyapp.Utils.DataEndPoints;
import com.matrackinc.spyapp.Utils.DataUtil;
import com.matrackinc.spyapp.Utils.DataUtilCallback;
import com.matrackinc.spyapp.Utils.DatabaseUtil;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tv1 = findViewById(R.id.text);
        TextView tv2 = findViewById(R.id.text1);
        TextView tv3 = findViewById(R.id.text2);
        TextView tv4 = findViewById(R.id.text3);

        String test = "activity::call###startTime::12/09/2017 14:32:00###endTime::12/09/2017 14:33:00";

        DataUtil dataUtil = new DataUtil(this);
        dataUtil.saveData(DataEndPoints.saveData, null, test, new DataUtilCallback() {
            @Override
            public void onCallbackComplete(boolean success) {
                //
            }
        });

        DataBaseClass db = new DataBaseClass(this);
        List<DataRecord> records = db.getNonUploadedRecords();
        for(DataRecord record : records){
            tv1.setText(Integer.toString(record.getId()));
            tv2.setText(record.getData());
            tv3.setText(record.getCreated_time());
            tv4.setText(record.getUploaded().toString());
        }
    }
}
